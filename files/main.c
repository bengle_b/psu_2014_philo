/*
** main.c for main in /home/bengle_b/rendu/PSU_2014_philo/files
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Sat Feb 28 17:51:41 2015 Bengler Bastien
** Last update Sun Mar  1 01:01:28 2015 Bengler Bastien
*/

#include "my.h"

int		fill_list(t_list *list)
{
  static int	i = 0;
  t_list	*tmp;

  if ((putInList(list)) == -1)
    return (-1);
  if ((pthread_create(&(list->prev->philo), NULL, loop_philo, list->prev)) != 0)
    return (-1);
  if (i++ < NB_PHILO - 1 && ((fill_list(list)) == -1))
    return (-1);
  return (0);
}

int		join_thread(t_list *list)
{
  list = list->next;
  while (list->root == 1)
    {
      pthread_join(list->philo, NULL);
      list = list->next;
    }
}

int		main()
{
  t_list	*list;

  if (NB_PHILO < 3)
    return (-1);
  if ((list = init_list(list)) == NULL)
    return (-1);
  join_thread(list);
}
