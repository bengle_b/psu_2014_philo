/*
** double_list.h for double_list in /home/bengle_b/tp/PSU/philosophes/files
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Mon Feb 23 16:46:31 2015 Bengler Bastien
** Last update Sun Mar  1 02:44:13 2015 Bengler Bastien
*/

#ifndef MY_H
# define MY_H

#include <pthread.h>

#define TIME_SLEEP	0
#define TIME_THINK	2
#define TIME_EAT	1

#define REPAS		3
#define NB_PHILO	7

typedef enum State State;

enum			State
  {
    DEFAULT,
    EAT,
    SLEEP,
    THINK
  };

typedef struct		s_list
{
  int			root;
  int			id;
  int			eat;
  State			state;
  pthread_t		philo;
  pthread_mutex_t	baguette;
  struct s_list		*prev;
  struct s_list		*next;
}			t_list;

void			*init_list(t_list *list);
int			putInList(t_list *list);
int			fill_list(t_list *list);
int			create_philo(void);
void			*loop_philo(void *arg);
int			getSize(t_list *list);

#endif	/* !MY_H */
