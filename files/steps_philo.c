/*
** steps_philo.c for steps_philo in /home/bengle_b/rendu/PSU_2014_philo
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Sat Feb 28 18:08:25 2015 Bengler Bastien
** Last update Sun Mar  1 02:42:53 2015 Bengler Bastien
*/

#include <pthread.h>
#include <stdio.h>
#include "my.h"

int		eat(t_list *list)
{
  if (((pthread_mutex_trylock(&(list->next->baguette))) == 0) &&
      ((pthread_mutex_trylock(&(list->baguette))) == 0))
    {
      list->state = EAT;
      sleep(TIME_EAT);
      printf("Philosophe %d is eating, %d times more !\n",
	     list->id, REPAS - list->eat);
      list->eat++;
    }
  else
    {
      pthread_mutex_unlock(&(list->next->baguette));
      pthread_mutex_unlock(&(list->baguette));
      return (-1);
    }
  return (0);
}

int		sleep_(t_list *list)
{
  pthread_mutex_unlock(&(list->next->baguette));
  pthread_mutex_unlock(&(list->baguette));
  list->state = SLEEP;
  printf("Philosophe %d is sleeping for %d seconds.\n",
	 list->id, TIME_SLEEP);
  sleep(TIME_SLEEP);
  return (0);
}

int		think(t_list *list)
{
  pthread_mutex_unlock(&(list->baguette));
  list->state = THINK;
  printf("Philosophe %d is thinking for %d seconds ...\n",
	 list->id, TIME_THINK);
}

void		*loop_philo(void *arg)
{
  t_list	*list;
  unsigned int	seed;
  int		my_rand;

  list = (t_list*)arg;
  seed = list->id;
  srand(seed);
  printf("Philosophe %d is ready !\n", list->id);
  my_rand = rand() % 2;
  while (list->eat < REPAS)
    {
      if ((eat(list)) == -1)
	sleep_(list);
      if (list->next->state == THINK || list->prev->state == THINK)
	think(list);
      else if ((list->next->root == 0 && list->next->next->state == THINK) ||
	       list->prev->state == THINK)
	think(list);
      else if ((list->prev->root == 0 && list->prev->prev->state == THINK) ||
	       list->next->state == THINK)
	think(list);
      else
	think(list);
    }
}
