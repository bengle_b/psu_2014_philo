/*
** double_list.c for double_list in /home/bengle_b/tp/PSU/philosophes/files
** 
** Made by Bengler Bastien
** Login   <bengle_b@epitech.net>
** 
** Started on  Mon Feb 23 16:46:21 2015 Bengler Bastien
** Last update Sun Mar  1 00:07:31 2015 Bengler Bastien
*/

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include "my.h"

void		*init_list(t_list *list)
{
  if ((list = malloc(sizeof(t_list))) == NULL)
    return (NULL);
  list->root = 0;
  list->state = DEFAULT;
  list->next = list;
  list->prev = list;
  list->eat = 0;
  list->id = 0;
  if ((fill_list(list)) == -1)
    return (NULL);
  return (list);
}

int		putInList(t_list *list)
{
  t_list	*tmp;
  t_list	*tmp2;

  tmp = list->prev;
  if ((tmp2 = malloc(sizeof(t_list))) == NULL)
    return (-1);
  tmp2->root = 1;
  tmp2->prev = tmp;
  tmp2->state = DEFAULT;
  tmp2->next = list;
  tmp2->eat = 0;
  tmp2->id = list->prev->id + 1;
  list->prev = tmp2;
  tmp->next = tmp2;
  pthread_mutex_init(&(tmp2->baguette), NULL);
  pthread_mutex_unlock(&(tmp2->baguette));
  return (0);
}

int		getSize(t_list *list)
{
  t_list	*tmp;
  int		size;

  tmp = list->next;
  while (tmp->root == 1)
    {
      size++;
      tmp = tmp->next;
    }
  return (size);
}
