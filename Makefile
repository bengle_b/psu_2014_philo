##
## Makefile for Makefile in /home/bengle_b/rendu/psu_2014_malloc
## 
## Made by Bengler Bastien
## Login   <bengle_b@epitech.net>
## 
## Started on  Fri Jan 30 11:25:24 2015 Bengler Bastien
## Last update Sat Feb 28 21:21:15 2015 Bengler Bastien
##

NAME		= philo

SRC		= files/main.c \
		  files/double_list.c \
		  files/steps_philo.c

OBJ		= $(SRC:.c=.o)

CC		= gcc

RM		= rm -f

#CFLAGS		= -Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(OBJ) -o $(NAME) -lpthread

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: clean fclean re all
